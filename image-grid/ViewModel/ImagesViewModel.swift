//
//  ImagesViewModel.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import Foundation
import XMLParsing


class ImagesViewModel {
    
    // MARK: - Delegate
    var delegate: ViewControllerProtocol?
    
    // MARK: - Properties
    #if DEBUG // make it public for tests
    var response: Data? {
        didSet {
            guard let data = response else { return }
            
            let decoder = XMLDecoder()

            do {
                let res = try decoder.decode(Response.self, from: data)
                self.finalResponse = res
            } catch {
               print(error)
            }
        }
    }
    #else // make it private for production
    private var response: Data? {
        didSet {
            guard let data = response else { return }
            
            let decoder = XMLDecoder()

            do {
                let res = try decoder.decode(Response.self, from: data)
                self.finalResponse = res
            } catch {
                self.error = error
            }
        }
    }
    #endif
    
    var photos: [Photo] = []
    
    private var additionalResponse: Data? {
        didSet {
            guard let data = additionalResponse else { return }
            
            let decoder = XMLDecoder()

            do {
                let res = try decoder.decode(Response.self, from: data)
                if let existingPhotos = res.photos?.photo {
                    for photo in existingPhotos {
                        photos.append(photo)
                    }
                }
            } catch {
                self.error = error
            }
            isAllowedToFetch = true
            delegate?.didFetchImages()
        }
    }
    
    var finalResponse: Response? {
        didSet {
            guard let res = finalResponse else { return }
            currentPage = res.photos?.page
            lastPage = Int(res.photos?.pages ?? "1")
            if let existingPhotos = res.photos?.photo {
                for photo in existingPhotos {
                    photos.append(photo)
                }
            }
            isAllowedToFetch = true
            delegate?.didFetchImages()
        }
    }
    
    var currentPage: String? {
        didSet {
            guard let currentPage = currentPage else { return }
            pageNumber = Int(currentPage) ?? 1
        }
    }
    
    var pageNumber = 1
    
    private var query: String!
    
    private var lastPage: Int!
    
    var isAllowedToFetch = false

    var error: Error? {
        didSet {
            guard let error = error else { return }
            delegate?.showError(error)
        }
    }
    
    var isLoading: Bool? {
        didSet {
            guard let isLoading = isLoading else { return }
            delegate?.setLoading(isLoading)
        }
    }

    #if DEBUG // make it public for tests
    var dataService: GetImagesService?
    #else // make it private for production
    private var dataService: GetImagesService?
    #endif
    
    // MARK: - Constructor
    init(dataService: GetImagesService) {
        self.dataService = dataService
    }
    
    func fetchImages(withParameters params: [String: String]) {
        isLoading = true
        isAllowedToFetch = false
        if query != params["text"] ?? "" {
            pageNumber = 1
            photos.removeAll()
        }
        query = params["text"] ?? ""
        self.dataService?.fetchImages(withParams: params, completionHandler: { (response, error) in
            if let error = error {
                self.error = error
                self.isLoading = false
                return
            }
            
            self.error = nil
            self.isLoading = false
            self.response = response
        })
    }
    
    func fetchMore() {
        isLoading = true
        isAllowedToFetch = false
        let nextPage = pageNumber + 1
        if nextPage >= lastPage { return }
        let params: [String: String] = ["text": query, "safe_search": "1", "page": String(nextPage)]
        pageNumber = nextPage
        self.dataService?.fetchImages(withParams: params, completionHandler: { (response, error) in
            if let error = error {
                self.error = error
                self.isLoading = false
                return
            }
            
            self.error = nil
            self.isLoading = false
            self.additionalResponse = response
        })
    }

}
