//
//  UIView+dimentions.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import UIKit

extension UIView {
    var width: CGFloat {
        return self.frame.size.width
    }
    
    var height: CGFloat {
        return self.frame.size.height
    }
    
    var bottomY: CGFloat {
        return self.frame.maxY
    }
}
