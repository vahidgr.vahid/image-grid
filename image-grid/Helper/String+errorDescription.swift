//
//  String+errorDescription.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import Foundation

extension String: LocalizedError {
    public var errorDescription: String? { return self }
}
