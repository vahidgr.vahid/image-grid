//
//  String+add.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import Foundation

extension String {
    func add(_ methodToURL: String) -> String {
        return (self + "?method=" + methodToURL)
    }
    
    func addToken(_ APIToken: String) -> String {
        return (self + "&api_key=" + APIToken)
    }
    
    func addPage(_ page: String) -> String {
        return (self + "&page=" + page)
    }

    func addPhotoInfo(_ server_id: String, _ photo_id: String, _ secret: String) -> String {
        let url = self + server_id + "/" + photo_id + "_" + secret + ".jpg"
        return url
    }
}
