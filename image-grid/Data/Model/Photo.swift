//
//  Photo.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import Foundation

struct Photo : Codable {
    let id : String?
    let owner : String?
    let secret : String?
    let server : String?
    let farm : String?
    let title : String?
    let ispublic : String?
    let isfriend : String?
    let isfamily : String?

    enum CodingKeys: String, CodingKey {

        case id = "id"
        case owner = "owner"
        case secret = "secret"
        case server = "server"
        case farm = "farm"
        case title = "title"
        case ispublic = "ispublic"
        case isfriend = "isfriend"
        case isfamily = "isfamily"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        owner = try values.decodeIfPresent(String.self, forKey: .owner)
        secret = try values.decodeIfPresent(String.self, forKey: .secret)
        server = try values.decodeIfPresent(String.self, forKey: .server)
        farm = try values.decodeIfPresent(String.self, forKey: .farm)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        ispublic = try values.decodeIfPresent(String.self, forKey: .ispublic)
        isfriend = try values.decodeIfPresent(String.self, forKey: .isfriend)
        isfamily = try values.decodeIfPresent(String.self, forKey: .isfamily)
    }

}
