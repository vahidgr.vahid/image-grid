//
//  Response.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import Foundation

struct Response : Codable {
    let stat : String?
    let photos : Photos?

    enum CodingKeys: String, CodingKey {

        case stat = "stat"
        case photos = "photos"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        stat = try values.decodeIfPresent(String.self, forKey: .stat)
        photos = try values.decodeIfPresent(Photos.self, forKey: .photos)
    }

}
