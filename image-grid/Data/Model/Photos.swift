//
//  Photos.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import Foundation

struct Photos : Codable {
    let page : String?
    let pages : String?
    let perpage : String?
    let total : String?
    let photo : [Photo]?

    enum CodingKeys: String, CodingKey {

        case page = "page"
        case pages = "pages"
        case perpage = "perpage"
        case total = "total"
        case photo = "photo"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        page = try values.decodeIfPresent(String.self, forKey: .page)
        pages = try values.decodeIfPresent(String.self, forKey: .pages)
        perpage = try values.decodeIfPresent(String.self, forKey: .perpage)
        total = try values.decodeIfPresent(String.self, forKey: .total)
        photo = try values.decodeIfPresent([Photo].self, forKey: .photo)
    }

}
