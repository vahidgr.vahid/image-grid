//
//  GetImagesService.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import Alamofire

class GetImagesService {
    
    var header: [String: String]?
    var httpMethod: HTTPMethod?
    var contentType: String = "application/json"
    
    enum FlickrMethods: String {
        case photos = "flickr.photos.search"
    }
    
    // MARK: - Singleton
    static let shared = GetImagesService()
    
    
    // MARK: - Services
    func fetchImages(withParams params: [String: String], completionHandler: @escaping (Data?, Error?) -> ()) {
        let page_number = params["page"] ?? "1"
        guard let url = URL(string: Global.base_url.add(FlickrMethods.photos.rawValue).addToken(Global.api_key).addPage(page_number)) else {
            do {
                try self.throwError(errorDescription: "URL is not Valid.")
            } catch {
                completionHandler(nil, error.localizedDescription)
            }

            return
        }
        
        header = ["accept": "application/json", "Content-Type": contentType]

        AF.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: .init(header!), interceptor: nil).response { dataResponse in
                switch dataResponse.result {
                case .success(let value):
                    if dataResponse.response?.statusCode == 200 {
                        if let reponseData = value {
                            completionHandler(reponseData, nil)
                        } else {
                            do {
                                try self.throwError(errorDescription: "Status code is okay but the results are empty :(")
                            } catch {
                                completionHandler(nil, error.localizedDescription)
                            }
                            return
                        }
                    } else {
                        let statusCode = dataResponse.response?.statusCode ?? -1
                        do {
                            try self.throwError(errorDescription: "Something went wrong: \(statusCode)")
                        } catch {
                            completionHandler(nil, error.localizedDescription)
                        }
                    }
                case .failure(let error):
                    print(error)
                    completionHandler(nil, error.localizedDescription)
                }
        }

    }

    
    // MARK: - Throw errors
    func throwError(errorDescription: String) throws {
        throw errorDescription
    }

}
