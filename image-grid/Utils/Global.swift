//
//  Global.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import Foundation

struct Global {
    public static let base_url = "https://api.flickr.com/services/rest/"
    public static let api_key = "ff068f8931ff91ce675a47458bf737fb"
    public static let secret = "9f7d7108b52e63f7"
    public static let image_url = "https://live.staticflickr.com/"
}
