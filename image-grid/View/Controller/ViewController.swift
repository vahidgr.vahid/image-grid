//
//  ViewController.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import UIKit
import ImageLoader

protocol ViewControllerProtocol {
    func didFetchImages()
    func showError(_ error: Error)
    func setLoading(_ isLoading: Bool)
}

class ViewController: UIViewController {
    
    // ViewModel
    let viewModel = ImagesViewModel(dataService: GetImagesService())
    
    // Next View Controller
    let imageViewController = ImageViewController()
    
    // TextField
    weak var textField: TextField!
    
    // CollectionView
    var collectionView: CollectionView!
    
    // Text label
    weak var label: Label!
    
    // Loading Views
    override func loadView() {
        super.loadView()
        
        let guide = view.safeAreaLayoutGuide

        // setup text field
        let textField = TextField()//(frame: CGRect(x: 20, y: topPadding, width: view.width - 40, height: 44))
        view.addSubview(textField)
        
        textField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            textField.topAnchor.constraint(equalTo: guide.topAnchor),
            textField.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 20),
            textField.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: -20),
            textField.heightAnchor.constraint(equalToConstant: 44),
        ])
        textField.delegate = self
        
        // setup collection view flow layout
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .vertical
        flowLayout.itemSize = CGSize(width: view.width / 3 - 15, height: view.width / 3 - 15)
        flowLayout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)

        // setup collection view
        let collectionView = CollectionView(collectionViewLayout: flowLayout)
        view.addSubview(collectionView)
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 10),
            collectionView.leadingAnchor.constraint(equalTo: guide.leadingAnchor, constant: 20),
            collectionView.trailingAnchor.constraint(equalTo: guide.trailingAnchor, constant: -20),
            collectionView.bottomAnchor.constraint(equalTo: guide.bottomAnchor),
        ])
        collectionView.identifier = Constants.identifier
        collectionView.delegate = self
        collectionView.dataSource = self
        
        // setup status label
        let label = Label()
        view.addSubview(label)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: guide.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: guide.centerYAnchor),
            label.widthAnchor.constraint(equalTo: guide.widthAnchor, multiplier: 0.8),
            label.heightAnchor.constraint(equalToConstant: 40),
        ])
        label.status = .waiting
        
        // assign created views to their references
        self.textField = textField
        self.collectionView = collectionView
        self.label = label

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set delegate and init fetching
        viewModel.delegate = self
    }
}


// MARK: - Network handeling
extension ViewController: ViewControllerProtocol {
    func didFetchImages() {
        // Reload data if there was non | insert data otherwise
        if viewModel.pageNumber < 2 { collectionView.reloadData() } else {
            let indexSet = IndexSet(integer: 0)
            collectionView.reloadSections(indexSet)
        }
        if viewModel.photos.count < 1 { label.status = .failed } else { label.status = .successful }
    }
    
    func search(withQuery query: String) {
        viewModel.fetchImages(withParameters: ["text": query, "safe_search": "1"])
        label.status = .loading
    }
    
    func setLoading(_ isLoading: Bool) {
        if isLoading { collectionView.alpha = 0 } else { collectionView.alpha = 1 }
    }
    
    func showError(_ error: Error) {
        let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Retry", style: .default) { _ in
            if let query = self.textField.text {
                self.viewModel.fetchImages(withParameters: ["text": query, "safe_search": "1"])
                self.label.status = .loading
            } else {
                self.label.status = .waiting
            }
        })
        alert.addAction(UIAlertAction(title: "Okay", style: .cancel) { _ in })
        self.present(alert, animated: true){}
    }
}

// MARK: - Search handeling
extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        guard let text = textField.text else { return false }
        search(withQuery: text)
        return false
    }
}

// MARK: - Setup collectionView
extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.identifier, for: indexPath) as! CollectionViewCell
        
        let dict = viewModel.photos[indexPath.row]
        
        if let server_id = dict.server, let photo_id = dict.id, let secret = dict.secret {
            let _image = Global.image_url.addPhotoInfo(server_id, photo_id, secret)
            cell.imageView.load.request(with: _image, onCompletion: { (image, error, operation) in
                DispatchQueue.main.async {
                    if let image = image {
                        cell.setImage(image)
                    }
                }
            })
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = viewModel.photos[indexPath.row]
        if let server_id = dict.server, let photo_id = dict.id, let secret = dict.secret {
            let image = Global.image_url.addPhotoInfo(server_id, photo_id, secret)
            
            
            imageViewController.imageURL = image
            imageViewController.backgroundColor = view.backgroundColor
            self.present(imageViewController, animated: true, completion: nil)
            
        }
    }
    
    // Fetch more images if there is more
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let midX: CGFloat = collectionView.bounds.midX
        let midY: CGFloat = collectionView.bounds.midY
        let point: CGPoint = CGPoint(x: midX, y: midY)
        
        guard let indexPath: IndexPath = collectionView.indexPathForItem(at: point) else { return }
        
        let currentPage: Int = indexPath.item
        if currentPage >= (viewModel.pageNumber * 90) {
            if !(viewModel.isLoading == true) {
                if viewModel.isAllowedToFetch {
                    viewModel.fetchMore()
                }
            }
        }
    }
}
