//
//  ImageViewController.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import UIKit
import ImageLoader

class ImageViewController: UIViewController {
    
    var imageURL: String!
    var backgroundColor: UIColor!
    weak var scrollView: ScrollView!
    
    override func loadView() {
        super.loadView()
        
        let scrollView = ScrollView()//(frame: CGRect(x: 0, y: 0, width: view.width, height: view.height))
        view.addSubview(scrollView)
        
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            scrollView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            scrollView.widthAnchor.constraint(equalTo: view.widthAnchor),
            scrollView.heightAnchor.constraint(equalTo: view.heightAnchor),
        ])
        
        scrollView.delegate = self

        scrollView.imageView.load.request(with: imageURL, onCompletion: { (image, error, operation) in
            DispatchQueue.main.async { [weak self] in
                guard let strongSelf = self, let image = image else { return }
                strongSelf.scrollView.setImage(image)
            }
        })
        
        scrollView.reduceAlpha(withColor: backgroundColor)
        
        self.scrollView = scrollView
    }
}

extension ImageViewController: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.scrollView.imageView
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        UIView.animate(withDuration: 0.3) {
            scrollView.zoomScale = 1.0
        }
    }
}
