//
//  Label.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import UIKit

class Label: UILabel {
    
    enum Mode: String {
        case waiting = "Search for results"
        case loading = "Loading data"
        case failed = "Failed to fetch data"
        case successful = ""
    }
    
    var status: Mode? {
        didSet {
            guard let status = status else { return }
            text = status.rawValue
        }
    }
    
    init() {
        super.init(frame: .zero)
        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    func setupView() {
        textAlignment = .center
        accessibilityIdentifier = "status_label"
    }
    
}
