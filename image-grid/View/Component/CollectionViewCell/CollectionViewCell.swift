//
//  CollectionViewCell.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    weak var imageView: UIImageView!
    
    init() {
        super.init(frame: .zero)
//        setupView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView?.image = nil
    }
    
    private func setupView() {
        layer.masksToBounds = true
        layer.cornerRadius = 5
        
        let imageView = UIImageView()
        contentView.addSubview(imageView)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            imageView.widthAnchor.constraint(equalTo: contentView.widthAnchor),
            imageView.heightAnchor.constraint(equalTo: contentView.heightAnchor),
        ])
        
        addSubview(imageView)
        
        self.imageView = imageView
    }
    
    func setImage(_ image: UIImage) {
        imageView?.image = image
        imageView?.contentMode = .scaleAspectFill
    }

}
