//
//  CollectionView.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import UIKit

class CollectionView: UICollectionView {
    
    var identifier: String? {
        didSet {
            guard let identifier = identifier else { return }
            register(CollectionViewCell.self, forCellWithReuseIdentifier: identifier)
        }
    }
    
    init(collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: .zero, collectionViewLayout: layout)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    private func setupView() {
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        backgroundColor = .clear
    }
    
}
