//
//  ScrollView.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import UIKit

class ScrollView: UIScrollView {
    
    weak var imageView: UIImageView!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imageView?.layer.cornerRadius = 11.0
    }
    
    init() {
        super.init(frame: .zero)
        setupView()
    }
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    func setupView() {
        alwaysBounceVertical = false
        alwaysBounceHorizontal = false
        showsVerticalScrollIndicator = false
        showsHorizontalScrollIndicator = false
        flashScrollIndicators()
        minimumZoomScale = 1.0
        maximumZoomScale = 10.0
        
        let imageView = UIImageView()
        addSubview(imageView)
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor),
            imageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            imageView.widthAnchor.constraint(equalTo: widthAnchor),
            imageView.heightAnchor.constraint(equalTo: heightAnchor),
        ])
        
        imageView.clipsToBounds = false
        self.imageView = imageView
    }
    
    func setImage(_ image: UIImage) {
        imageView?.image = image
        imageView?.contentMode = .scaleAspectFit
    }
    
    func reduceAlpha(withColor color: UIColor) {
        UIView.animate(withDuration: 1.0) { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.backgroundColor = color.withAlphaComponent(0.6)
        }
    }
        
}
