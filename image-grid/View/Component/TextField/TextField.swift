//
//  TextField.swift
//  image-grid
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import UIKit

class TextField: UITextField {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = 8
        textAlignment = .center
        placeholder = "Search"
        backgroundColor = .orange
        
    }
    
}
