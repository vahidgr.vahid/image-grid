This application is for ApAiTech interview assessment

### I've used MVVM to structure code base. Here is how it works:

- App -> AppDelegate and main.storyboard
- Data -> services and models
    - Service -> handle API call
    - Model -> Codable model
- Helper -> extensions for classes and components
- Utils -> global variables set across the project for example base api
- View -> controllers and components
    - Controller -> manage views and view models
    - Component -> custom made UIViews
- ViewModel -> view models manage services and use models to create an appropriate result for controllers to feed views.

### Pods used in project:
- [Alamofire](https://github.com/Alamofire/Alamofire) -> Handles networking.
- [XMLParsing](https://github.com/ShawnMoore/XMLParsing) -> Handles XML response given by API and creates Codable models.
- [ImageLoader](https://github.com/hirohisa/ImageLoaderSwift) -> Downloads and saves images into UserDefaults. Loads images into appropriate UIImageView


### XCT
- [x] UI tests
- [x] Unit tests
