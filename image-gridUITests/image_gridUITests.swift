//
//  image_gridUITests.swift
//  image-gridUITests
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import XCTest

class image_gridUITests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launchArguments = ["enable-testing"]
        app.launch()

        app.textFields.element.tap()
        
        let my_labels = app.staticTexts.matching(identifier: "status_label")
        if my_labels.count > 0 {
            let status_label = my_labels.element(boundBy: 0)
            XCTAssertTrue(status_label.label == "Search for results")
        }

        app.keys["C"].tap() // if failes here, try to toggle software keyboard on your simulator
        app.keys["a"].tap()
        app.keys["t"].tap()
        app.keys["s"].tap()
        app.keyboards.buttons["Return"].tap()

        
        let status_label = my_labels.element(boundBy: 0)
        XCTAssertTrue(status_label.label == "Loading data")
        
        // wait for collection view to reload data
        let firstImage = app.collectionViews.children(matching:.any).element(boundBy: 0)
        let imageExists = firstImage.waitForExistence(timeout: 10.0)
        if imageExists {
            firstImage.tap()
        }
        
        let image = app.scrollViews.element(boundBy: 0)
        image.pinch(withScale: 3, velocity: 4) // zoom in
        
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
