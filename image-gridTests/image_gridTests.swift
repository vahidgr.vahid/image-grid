//
//  image_gridTests.swift
//  image-gridTests
//
//  Created by Vahid Ghanbarpour on 12/14/20.
//

import XCTest
@testable import image_grid

class image_gridTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let url = Global.base_url.add("method").addPage("page").addToken("token")
        let urlToEqual = Global.base_url + "?method=method" + "&page=page" + "&api_key=token"
        XCTAssertEqual(url, urlToEqual)
        
        let image_url = Global.image_url.addPhotoInfo("server_id", "photo_id", "secret")
        let image_urlToEqual = Global.image_url + "server_id" + "/photo_id" + "_secret" + ".jpg"
        XCTAssertEqual(image_url, image_urlToEqual)

    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testSearchApiResponse() {
        let e = expectation(description: "Alamofire")
        
        let viewModel = ImagesViewModel(dataService: GetImagesService())
        
        viewModel.dataService?.fetchImages(withParams: ["text": "cats", "safe_search": "1"], completionHandler: { (response, error) in
            if let err = error {
                print(err.localizedDescription)
                e.fulfill()
                return
            }
            
            viewModel.response = response
            let resultString = viewModel.finalResponse?.stat
            let expectedString = "ok"
            XCTAssertEqual(resultString, expectedString)
            
            e.fulfill()
        })
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }

}
